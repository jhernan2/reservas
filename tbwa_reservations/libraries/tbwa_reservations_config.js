/* global Drupal, jQuery */
(function($) {
  'use strict';

  var NSReservas = {},
  obj1 = '.is-numeric',
  obj2 = '.filter-date',
  obj3 = '.active-emergency',
  obj4 = '.auditorium-structure',
  obj5 = '.auditorium-content tr';

  NSReservas = (function () {
    /* Private */
    var c1 = '#ce890b',
    c2 = '#6f6f6f',
    c3 = 'silver',
    c4 = '#911010',
    t1 = 0;

    /* Public */
    return {
      // Function to only enter the item numeric
      validateNumeric: function() {
        $(obj1).on('keypress paste', function(e) {
          if (e.type == 'paste') {
            e.preventDefault();
          }
          if(e.which < 46 || e.which > 59) {
            e.preventDefault();
          }
          if(e.which == 46 && $(this).val().indexOf('.') != -1) {
            e.preventDefault();
          }
        });
      },
      // Fields calendar
      fieldsCalendar: function() {
        $(obj2).datepicker();
      },
      // Set the emergency exit.
      setEmergencyExit: function($mainParent) {
        $(obj3).click(function(e) {
          e.preventDefault();

          if (t1 != e.timeStamp) {
            t1 = e.timeStamp;
            var $parent = $(this).parents(obj4); // Contenedor del auditorio
            var $rows = $parent.find(obj5); // Filas
            // Ultima fila, se suma 94 para obtener su valor ASCII
            var lastRow = ($rows.length) + 94;
            lastRow = String.fromCharCode(lastRow);

            // Agrega mensaje para informar al usuario como activar salidas de emerncia
            $('.msg-info').remove();
            var msg = '<div class="msg-info">Las ubicaciones disponibles para salida de emergencia estan activas. Click en la salida que quieres activar.</div>';
            $(this).after(msg);

            // Recorre cada fila de la tabla
            $.each($rows, function(key, value) {
              var $tds = $(this).find('td'); // grupo de td de cada tr en la tabla
              // Número de la ultima silla de la fila
              var lastChair = $tds.length - 2; // Se restan 2 para quitar td de pasillo y letra

              // Recorre cada silla (td) de la fila
              $.each($tds, function(k, v) {
                // Selecciona la silla (span) del td
                var $thisChair = $(this).find('span');
                // Para sillas deshabilitadas se activan como posibles salidas de emergencia
                if ($thisChair.hasClass('chair-disabled')) {
                  // Verifica que la posicion sea apta para salida de emergencia
                  // Debe estar en la fila A o la última fila
                  // Debe ser la primera silla o la última
                  if ($thisChair.hasClass('row-a') || $thisChair.hasClass('column-1') || $thisChair.hasClass('row-' + lastRow) || $thisChair.hasClass('column-' + lastChair)) {
                    $thisChair.addClass('posible-emer-exit');
                  }
                }
              });
            });

            // Intervalo de tiempo para solicitar la activacion de salidas de emergencia.
            var color = c1;
            var inc = 0;
            var interval = setInterval(function() {
              $('.posible-emer-exit').css({
                'background-color': color,
              });

              // Cambia el color dependiendo el actual.
              color = color == c1 ? c2 : c1;
              inc++;
              // Limite de iteracciones para restaurar los campos y quitar intervalo.
              if (inc >= 40) {
                clearInterval(interval);
                $('.posible-emer-exit').removeClass('posible-emer-exit').css({
                  'background-color': c3,
                });
              }
            }, 500);

            // Confirmar activacion de salida de emergencia.
            $('.posible-emer-exit').click(function() {
              // Remueve clases necesarias
              $(this).removeClass('posible-emer-exit'); // Remove clase de emergencia.
              $(this).removeClass('chair-disabled'); // Remueve clase de silla deshabilitada.
              // Add class to chair emergency.
              $(this).addClass('emergency-chair');
              $(this).css({
                'background-color': c4
              });

              Drupal.structureAuditorium($mainParent);
            });
          }
        });
      }
    }
  }());

  $(window).on('load', function() {
    // Active numerics fields
    if ($(obj1).length) {
      NSReservas.validateNumeric();
    }
    // Active calendars fiels
    if ($(obj2).length) {
      NSReservas.fieldsCalendar();
    }
  });

  Drupal.tbwaReservationsConfig = Drupal.tbwaReservationsConfig || {};
  Drupal.tbwaReservationsConfig.iter = 0;
  Drupal.behaviors.tbwaReservationsConfig = {
    attach: function(context, settings) {

      // Events on the auditorium configuration page
      if ($('body').hasClass('page-admin-config-system-auditoriums-config')) {
        $('a.btn-create-structure').click(function(e) {
          e.preventDefault();
          var errorExist = false;
          var errorMsg = '';
          var $parentContainer = $(this).parents('div.container-fields');
          var $parent = $parentContainer.parent();
          var rows = $parent.find('input.rows-floor').val();
          if ($('p.error-message').length) {
            $('p.error-message').remove();
          }

          // Validate the number of rows in the auditorium
          if (rows > 0 && rows <= 26) {
            var columns = $parent.find('input.columns-floor').val();
            if (columns > 0 && columns <= 40) {
              $parent.find('.container-structure').html('');

              // Send in ajax information for to gif structure HTML
              Drupal.callAjaxStructure($parent, rows, columns);
            }
            else {
              errorExist = true;
              errorMsg = 'El rango de columnas está entre 1 y 40';
            }
          }
          else {
            errorExist = true;
            errorMsg = 'El rango de filas entre 1 y 26';
          }

          if (errorExist) {
            $(this).after('<p class="error-message">' + errorMsg + '</p>');
          }
        });

        // Print th table structure
        if ($('input.structure-chairs')) {
          var $structures = $('input.structure-chairs');

          $.each($structures, function(key, value) {
            var $parent = $(this).parent();
            var rows = $parent.find('input.rows-floor').val();
            var columns = $parent.find('input.columns-floor').val();
            var orderChairs = $(this).val();

            // Send in ajax information for to gif structure HTML
            Drupal.callAjaxStructure($parent, rows, columns, orderChairs);
          });
        }
      }
    }
  }

  // Function to only enter the item number.
  Drupal.onlyNumeric = function(element) {
    element.on('keypress paste', function(e) {
      if (e.type == 'paste') {
        e.preventDefault();
      }
      if(e.which < 46 || e.which > 59) {
        e.preventDefault();
      }
      if(e.which == 46 && $(this).val().indexOf('.') != -1) {
        e.preventDefault();
      }
    });
  }

  // Ajx get structure table
  Drupal.callAjaxStructure = function($parent, rows, columns, orderChairs = null) {
    // Send in ajax information for to gif structure HTML
    $.ajax({
      url: Drupal.settings.basePath + 'post-ajax/get-structure-auditorium',
      type: 'POST',
      data: {
        'rows': rows,
        'columns': columns,
        'order': orderChairs
      },
      dataType: 'json',
      success: function(data, textStatus, jqXHR) {
        if (data) {
          if (!$parent.find('.container-structure').html()) {
            $parent.find('.container-structure').html(data);
            // To create the configuration of chairs actives and inactives
            Drupal.structureAuditorium($parent);
          }

          $('.item-corredor').click(function() {
            var fieldsToDisabled = $(this).attr('fieldstodisable');
            if ($parent.find('.' + fieldsToDisabled).hasClass('chair-disabled')) {
              $parent.find('.' + fieldsToDisabled).removeClass('chair-disabled');
            }
            else {
              $parent.find('.' + fieldsToDisabled).addClass('chair-disabled');
            }

            // To create the configuration of chairs actives and inactives
            Drupal.structureAuditorium($parent);
          });

          // Change the configuration chairs individually
          $('.item-chair').click(function(e) {
            if (Drupal.tbwaReservationsConfig.iter != e.timeStamp) {
              Drupal.tbwaReservationsConfig.iter = e.timeStamp;

              if ($(this).hasClass('chair-disabled')) {
                $(this).removeClass('chair-disabled');
              }
              else {
                $(this).addClass('chair-disabled');
              }

              // To create the configuration of chairs actives and inactives
              Drupal.structureAuditorium($parent);
            }
          });

          // Set emergency exit
          if ($(obj3).length) {
            NSReservas.setEmergencyExit($parent);
          }
        }
      }
    });
  }

  // Fields information
  Drupal.structureAuditorium = function($parent) {
    var rows = $parent.find('.auditorium-content tr');
    var groupChair = {}

    $.each(rows, function(key, value) {
      var $fieldsRow = $(this).find('td.is-chair');
      if ($fieldsRow.length) {
        groupChair[key] = {}
      }

      $.each($fieldsRow, function(k, v) {
        var $chair = $(this).find('span');

        if ($chair.hasClass('emergency-chair') || $chair.hasClass('emergency-exit')) {
          groupChair[key][k] = 'e';
        }
        else if ($chair.hasClass('chair-disabled')) {
          groupChair[key][k] = false;
        }
        else { 
          groupChair[key][k] = true;
        }
      });
    });

    // Assign configuration information to the field
    var data = JSON.stringify(groupChair);
    $parent.find('input.structure-chairs').val(data);
  }

})(jQuery)
