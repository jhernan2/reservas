(function($) {
  'use strict';

  Drupal.behaviors.tbwaConfigureReserve = {
    attach: function(context, settings) {
      var NSConfigureReserve = {},
      auditorium = '.auditorium-structure',
      chair = '.item-chair.is-chair',
      chairSelected = '.chair-selected',
      pageReservations = '.container-page-reservations';

      if ($(auditorium).length) {
        // Event information
        var data_event = Drupal.settings.data_event;
        var current_reservas = Drupal.settings.current_reservas;

        $(chair).click(function() {
          var limit = 0;
          var $currentChair = $(this);
          var $chairSelected = $(chairSelected);
          // Change limit if current page is reservations
          if ($(pageReservations).length) {
            limit = Drupal.settings.limit;
          }

          // Para saber si la cantidad de sillas seleccionadas son menor al limite de sillas
          // $chairSelected.length <= limit
          // Si el limite es igual a 0 se encuentra en una página de administracióm
          if (limit == 0 || current_reservas < limit && (($chairSelected.length <= limit) || $(this).hasClass('chair-selected'))) {
            var chairStatus = 0;
            if ($currentChair.hasClass('chair-empty')) {
              $currentChair.removeClass('chair-empty').addClass('chair-selected');
              chairStatus = 1;
            }
            else if(!$currentChair.hasClass('chair-busy')) {
              $currentChair.addClass('chair-empty').removeClass('chair-selected');
            }

            if(!$currentChair.hasClass('chair-busy')) {
              // Chair information
              var chair = $currentChair.attr('chair');
              var floor = $currentChair.attr('floor');

              $.ajax({
                url: Drupal.settings.basePath + 'post-ajax/reserve-chair',
                type: 'POST',
                data: {
                  'event': data_event,
                  'chair': chair,
                  'floor': floor,
                  'status': chairStatus,
                  'limit': limit
                },
                dataType: 'json',
                success: function(data, textStatus, jqHQR) {
                  if ($('.selected-' + chair).length) {
                    $('.selected-' + chair).remove();
                  }
                  else {
                    $('.selected-chairs.' + floor + ' .chairs').append(' <span class="selected-' + chair + '">' + chair + ',</span>');;
                  }
                }
              });
            }
          }
        });
      }

      // Mostrar el modal cuando existan reservaciones.
      if ($('.have-reservation').length && !$('#modalContent').length) {
        var showModal = setInterval(function() {
          clearInterval(showModal);
          $('.ctools-use-modal').click();
        }, 1000);
      }

      $('.form-item-floors input').change(function() {
        var contentId = $(this).attr('id');
        contentId = contentId.replace('edit-floors-', '.content-floor-');

        $('.floor-active').removeClass('floor-active').addClass('floor-inactive');
        $(contentId).addClass('floor-active').removeClass('floor-inactive');
      });
    }
  }

})(jQuery)
