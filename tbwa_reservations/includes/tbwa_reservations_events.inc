<?php

/**
 * @file
 * File to reservations in events.
 */

/**
 * Function tbwa_reservation_create_list().
 *
 * Create list of auditoriums.
 */
function tbwa_reservations_events($node) {
  // Event information.
  $nid = $node->nid;
  $eventTitle = $node->title;
  // Filters.
  $form = drupal_get_form('tbwa_reservations_create_filter_form', $nid);

  $build = [
    '#prefix' => '<div class="wrapper-page">',
    '#suffix' => '</div>',
    'summary' => [
      '#prefix' => '<div class="summary">',
      '#suffix' => '</div>',
      '#markup' => t('Reserve an auditorium for the @eventTitle event', ['@eventTitle' => $eventTitle]),
    ],
    'filters' => [
      '#prefix' => '<div class="wrapper-filters">',
      '#suffix' => '</div>',
      '#markup' => render($form),
    ],
    'content' => [
      '#prefix' => '<div id="content-auditoriums-list">',
      '#suffix' => '</div>',
    ],
  ];

  return $build;
}

/**
 * Implements hook_form().
 */
function tbwa_reservations_create_filter_form($form, &$form_state, $node_id) {
  $form_state['storage']['node_id'] = $node_id;
  // Date tommorrow.
  $year = date('Y');
  $month = date('n');
  $day = date('d') + 1;
  $def = array(
    'year' => $year,
    'month' => $month,
    'day' => $day,
  );

  // Filter to looking for date.
  $form['date'] = [
    '#type' => 'date',
    '#title' => t('Select the date of the event'),
    '#default_value' => $def,
  ];
  // Numbers hours to use.
  $form['hours'] = [
    '#type' => 'select',
    '#title' => t('Numbers to hours to use'),
    '#options' => [t('Select hours'), 1, 2, 3, 4, 5, 6, 7, 8],
  ];
  // Tipo de evento.
  $form['type'] = [
    '#type' => 'radios',
    '#title' => t('Select the event type'),
    '#options' => [
      TBWA_RESERVATIONS_TYPE_PRIVATE => t('Private'),
      TBWA_RESERVATIONS_TYPE_PUBLIC => t('Public'),
    ],
    '#default_value' => TBWA_RESERVATIONS_TYPE_PRIVATE,
  ];

  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Search'),
    '#ajax' => [
      'callback' => 'tbwa_reservations_create_filter_form_ajax',
      'wrapper' => 'content-auditoriums-list',
    ],
  ];

  return $form;
}

/**
 * Implementation of hook_form_submit().
 */
function tbwa_reservations_create_filter_form_validate($form, &$form_state) {
  unset($form_state['storage']['error']);
  $date = $form_state['values']['date'];
  $hours = $form_state['values']['hours'];
  extract($date);
  $date_send = strtotime("{$day}-{$month}-{$year}");

  if ($date_send < time()) {
    form_set_error('date', t('The date can not be less than the actual date'));
    $form_state['storage']['error'] = TRUE;
  }
  if (!$hours) {
    form_set_error('date', t('The number of hours is necessary'));
    $form_state['storage']['error'] = TRUE;
  }
}

/**
 * Implementation of hook_form_submit().
 */
function tbwa_reservations_create_filter_form_submit($form, &$form_state) {
  if (!isset($form_state['storage']['error'])) {
    $date = $form_state['values']['date'];
    $hours = $form_state['values']['hours'];
    $type = $form_state['values']['type'];
    extract($date);

    // Validate current time.
    $today = strtotime(date('j-m-Y'));
    $date_send = strtotime("{$day}-{$month}-{$year}");
    if ($today == $date_send) {
      $date_send = time();
    }

    // Get values to filter auditoriums.
    $form_state['storage']['date'] = $date_send;
    $form_state['storage']['hours'] = $hours;
    $form_state['storage']['type'] = $type;
  }
}

/**
 * Ajax callback.
 */
function tbwa_reservations_create_filter_form_ajax($form, &$form_state) {
  $build = [
    '#prefix' => '<div id="content-auditoriums-list">',
    '#suffix' => '</div>',
  ];
  if (!isset($form_state['storage']['error'])) {
    // Get the auditoriums.
    $type = $form_state['storage']['type'];
    $hours = $form_state['storage']['hours'];
    $date = $form_state['storage']['date'];
    $nid = $form_state['storage']['node_id'];
    $auditoriums = tbwa_reservations_events_list($nid, $date, $hours, $type);

    // Structure of the list auditoriums.
    $build['#markup'] = render($auditoriums);
  }

  return $build;
}

/**
 * Function tbwa_reservations_events_list($nid).
 *
 * List auditoriums.
 *
 * @param int $nid
 *   Node identificator.
 * @param string $date
 *   Event date in timestamp.
 * @param int $hours
 *   Hours.
 * @param string $type
 *   The event type.
 */
function tbwa_reservations_events_list($nid, $date, $hours, $type) {
  // Search for auditoriums after 24 hours.
  $date_start = date('F j, Y', $date);
  $hour_initial = date('H', $date) + 1;
  $hour_initial = $hour_initial < 8 ? 8 : $hour_initial;

  // Get the auditoriums.
  $querys = tbwa_reservations_load_querys();
  $auditoriums = $querys->getAuditoriums();
  $header = [
    t('Date'),
    t('Auditorium'),
    t('Number chairs'),
    t('Location'),
    t('Actions'),
  ];
  $rows = [];
  for ($i = $hour_initial; $i <= 20; $i++) {
    foreach ($auditoriums as $key => $value) {
      $auditorium_id = $value->id;
      $config = drupal_json_decode($value->config_chairs);
      $number_chairs = $config['number_chairs'];
      if ($number_chairs && $number_chairs > 0) {
        $date_reservation = "{$date_start} {$i}:00";
        $date_reservation_str = strtotime("{$date_start} {$i}:00");
        $date_end = $date_reservation_str + ($hours * 3600);
        $validate = tbwa_reservations_events_validate_auditorium($auditorium_id, $date_reservation_str, $date_end, $nid);

        if (!$validate) {
          $rows[] = [
            $date_reservation,
            $value->name,
            "{$number_chairs} " . t('Chairs'),
            $value->location,
            l(t('Reserve'), "reserve-auditorium/{$nid}/{$date_reservation_str}/{$auditorium_id}-{$hours}-{$type}"),
          ];
        }
      }
    }
  }

  return [
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  ];
}

/**
 * Function tbwa_reservations_events_configure().
 *
 * Structure to configure events.
 *
 * @param object $node
 *   The node.
 * @param string $date
 *   Time event in timestamp format.
 * @param int $auditorium_hours
 *   The hours.
 *
 * @return array
 *   Auditorium structure.
 */
function tbwa_reservations_events_configure($node, $date, $auditorium_hours) {
  // Get the auditorium information.
  list($auditorium_id, $hours, $type) = explode('-', $auditorium_hours);
  $querys = tbwa_reservations_load_querys();
  $auditorium = $querys->getAuditoriumsInfo($auditorium_id);

  $data_event = [
    'nid' => $node->nid,
    'date_ini' => $date,
    'date_fin' => $date + ($hours * 3600),
    'auditorium_id' => $auditorium_id,
    'type' => $type,
  ];
  $create = tbwa_reservations_events_create($data_event);

  if ($create) {
    // Get the auditorium structure.
    $rooms = tbwa_reservations_auditorium_structure($auditorium_id, $data_event);

    // Build the page structure.
    $build = [
      '#prefix' => '<div class="wrapper-page page-configure-reserve">',
      '#suffix' => '</div>',
      'event' => [
        '#prefix' => '<div class="event-information">',
        '#suffix' => '</div>',
        'name' => [
          '#prefix' => '<div class="event-name">',
          '#suffix' => '</div>',
          '#markup' => '<b>' . t('Event name') . ':</b> ' . $node->title,
        ],
        'auditorium' => [
          '#prefix' => '<div class="auditorium-name">',
          '#suffix' => '</div>',
          '#markup' => '<b>' . t('Auditorium') . ':</b> ' . $auditorium->name,
        ],
        'date' => [
          '#prefix' => '<div class="event-date">',
          '#suffix' => '</div>',
          '#markup' => '<b>' . t('Date') . ':</b> ' . date('F j, Y h:i a', $date) . date(' - h:i a', $data_event['date_fin']),
        ],
        'demo_chairs' => [
          '#prefix' => '<div class="demo-chairs">',
          '#suffix' => '</div>',
          'description' => [
            '#prefix' => '<p>',
            '#suffix' => '</p>',
            '#markup' => t('Click on the chair to reserve it, at the end you click in finish reserve'),
          ],
          'empty' => [
            '#prefix' => '<div class="wrapper-empty">',
            '#suffix' => '</div>',
            '#markup' => '<span class="item-chair demo-empty"></span>  ' . t('Chairs available'),
          ],
          'busy' => [
            '#prefix' => '<div class="wrapper-busy">',
            '#suffix' => '</div>',
            '#markup' => '<span class="item-chair demo-busy"></span>  ' . t('Chairs busy'),
          ],
        ],
        'link_finish' => [
          '#prefix' => '<div class="finish-reserve">',
          '#suffix' => '</div>',
          '#markup' => l(t('Finish reserve'), 'reserve-auditorium/finish/' . $node->nid),
        ],
      ],
      'auditorium' => [
        '#prefix' => '<div class="wrapper-auditorium">',
        '#suffix' => '</div>',
        '#markup' => render($rooms),
      ],
    ];
  }
  else {
    $build = [
      '#prefix' => '<div class="auditorium-alert-error">',
      '#suffix' => '</div>',
      '#markup' => t('The auditorium could not be reserved'),
    ];
  }

  return $build;
}

/**
 * Function tbwa_reservations_events_create().
 *
 * Create or update the reservetion.
 *
 * @param array $data_event
 *   The event information.
 *
 * @return bool
 *   Return boolean for know if it was registred
 */
function tbwa_reservations_events_create(array $data_event = NULL) {
  if (!empty($data_event)) {
    extract($data_event);

    // Validate if the auditorium is busy.
    $val = tbwa_reservations_events_validate_auditorium($auditorium_id, $date_ini, $date_fin, $nid);
    if (!$val) {
      $time = time();
      // Data to save.
      $data = [
        'start_date' => $date_ini,
        'finish_date' => $date_fin,
        'type' => $type,
        'auditorium_id' => $auditorium_id,
        'node_id' => $nid,
        'modified' => $time,
      ];
      // Register the event.
      if (!tbwa_reservations_events_validate_exist($data_event['nid'])) {
        $data['created'] = $time;

        drupal_write_record('tbwa_events', $data);
      }
      // Update the event.
      else {
        db_update('tbwa_events')
          ->fields($data)
          ->condition('node_id', $nid)
          ->execute();
      }

      return TRUE;
    }
    // The auditorium could not be reserved.
    else {
      return FALSE;
    }
  }
}

/**
 * Function tbwa_reservations_events_validate_exist().
 *
 * Validate existing a reservation to the event.
 *
 * @param int $node_id
 *   Node identificator.
 *
 * @return bool
 *   The query.
 */
function tbwa_reservations_events_validate_exist($node_id) {
  $query = db_select('tbwa_events', 'e')
    ->fields('e')
    ->condition('node_id', $node_id)
    ->countQuery()
    ->execute()->fetchField();

  return $query;
}

/**
 * Function tbwa_reservations_events_validate_exist().
 *
 * Validate existing a reservation to the event.
 *
 * @param int $auditorium_id
 *   The auditorium identificator.
 * @param int $date_ini
 *   The date initial.
 * @param int $date_fin
 *   The date end.
 * @param int $nid
 *   The node identificator.
 *
 * @return bool
 *   The query.
 */
function tbwa_reservations_events_validate_auditorium($auditorium_id, $date_ini, $date_fin, $nid) {
  $validate = FALSE;
  // 1 hour is added to validate the occupation of the auditorium.
  $date_ini -= 3600;
  $date_fin += 3600;
  while ($date_ini <= $date_fin) {
    // Search reservations for the public at the time.
    $or = db_or();
    $or->condition('start_date', [$date_ini, $date_fin], 'BETWEEN');
    $or->condition('finish_date', [$date_ini, $date_fin], 'BETWEEN');

    $validate = db_select('tbwa_events', 'e')
      ->fields('e')
      ->condition('auditorium_id', $auditorium_id)
      ->condition($or)
      ->condition('node_id', $nid, '<>')
      ->countQuery()
      ->execute()->fetchField();

    // The auditorium is busy.
    if ($validate) {
      break;
    }

    // Next hour.
    $date_ini += 3600;
  }

  return $validate;
}

/**
 * Implements hook_form().
 */
function tbwa_reservations_events_finish_reserve_form($form, &$form_state, $nid) {
  $eventId = tbwa_reservations_get_id_by_node($nid);
  $reservations = tbwa_reservations_get_by_event($eventId);

  $form['description'] = [
    '#markup' => t('Please complete the following information to finish the reserve'),
  ];
  // Fields to reservations.
  $form['reservations'] = [
    '#type' => 'container',
    'fields' => [
      '#tree' => TRUE,
    ],
  ];
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Finish'),
  ];

  foreach ($reservations as $key => $value) {
    $form['reservations']['fields'][] = [
      '#type' => 'fieldset',
      '#title' => t('Floor @floor - Chair @chair', ['@floor' => $value->floor, '@chair' => $value->chair]),
      'identification' => [
        '#type' => 'textfield',
        '#title' => t('Identification'),
        '#attributes' => ['placeholder' => t('Identification')],
        '#default_value' => $value->identification,
        '#required' => TRUE,
      ],
      'name' => [
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#attributes' => ['placeholder' => t('Name')],
        '#default_value' => $value->name,
        '#required' => TRUE,
      ],
      'lastname' => [
        '#type' => 'textfield',
        '#title' => t('Lastname'),
        '#attributes' => ['placeholder' => t('Lastname')],
        '#default_value' => $value->lastname,
        '#required' => TRUE,
      ],
      'email' => [
        '#type' => 'textfield',
        '#title' => t('Email'),
        '#attributes' => ['placeholder' => t('Email')],
        '#default_value' => $value->email,
        '#required' => TRUE,
      ],
      'id' => [
        '#type' => 'hidden',
        '#value' => $value->id,
      ],
    ];
  }

  return $form;
}

/**
 * Implements hook_form().
 */
function tbwa_reservations_events_finish_reserve_form_submit($form, &$form_state) {
  if (isset($form_state['values']['fields'])) {
    $fields = $form_state['values']['fields'];

    foreach ($fields as $key => $value) {
      $value['status'] = TBWA_RESERVATIONS_RESERVE_BY_CONFIRM;
      db_update('tbwa_reservations')
        ->fields($value)
        ->condition('id', $value['id'])
        ->execute();
    }

    // Successfully message.
    drupal_set_message(t('The reservations has been finish successfully'));
  }
}
