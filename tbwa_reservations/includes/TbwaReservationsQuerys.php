<?php

/**
 * @files
 *
 * Querys of module.
 */
class TbwaReservationsQuerys {

  /**
   * Create object TbwaReservationsQuerys.
   */
  public function __construct() {}

  /**
   * Get Auditoriums.
   */
  public function getAuditoriums() {
    $query = db_select('tbwa_auditoriums', 'a')
      ->fields('a')
      ->orderBy('name', 'ASC')
      ->execute()->fetchAll();

    return $query;
  }

  /**
   * Get Auditoriums information.
   */
  public function getAuditoriumsInfo(int $id) {
    $query = db_select('tbwa_auditoriums', 'a')
      ->fields('a')
      ->condition('id', $id)
      ->orderBy('id', 'DESC')
      ->execute()->fetchObject();

    return $query;
  }

  /**
   * Get Events.
   */
  public function getEvents() {
    $query = db_select('tbwa_events', 'e')
      ->fields('e')
      ->orderBy('start_date', 'ASC')
      ->execute()->fetchAll();

    return $query;
  }

  /**
   * Get Events with filters.
   */
  public function getEventsFilter($filters) {
    $query = db_select('tbwa_events', 'e')
      ->fields('e');

    foreach ($filters as $key => $info) {
      $query->condition($info['field'], $info['value'], $info['oper']);
    }

    $result = $query->orderBy('start_date', 'ASC')
      ->execute()->fetchAll();

    return $result;
  }

}
