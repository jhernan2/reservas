<?php

/**
 * @file
 * Page configuration reserves.
 */

/**
 * Implements hook_form().
 */
function tbwa_reservations_conf_auditoriums_form($form, &$form_state, $auditorium_id = NULL) {
  drupal_add_js(drupal_get_path('module', 'tbwa_reservations') . '/libraries/tbwa_reservations_config.js');
  drupal_add_css(drupal_get_path('module', 'tbwa_reservations') . '/libraries/tbwa_reservations_config.css');

  if (isset($auditorium_id)) {
    // Set the auditorium Id in storage.
    $form_state['storage']['auditorium_id'] = $auditorium_id;

    // Get the auditorium configuration.
    $querys = tbwa_reservations_load_querys();
    $auditorium = $querys->getAuditoriumsInfo($auditorium_id);
    if (!empty($auditorium)) {
      $auditorium = (array) $auditorium;
      extract($auditorium);

      // Configuration information.
      $config = drupal_json_decode($config_chairs);
      $number_floors = 0;
      foreach ($config as $key => $value) {
        if (is_array($value)) {
          $number_floors++;
        }
      }
    }
  }

  // Fields to configure general information.
  $form['config_general'] = [
    '#type' => 'fieldset',
    '#title' => t('General information'),
    'name' => [
      '#type' => 'textfield',
      '#title' => t('Auditorium name'),
      '#attributes' => [
        'placeholder' => t('Enter the auditorium name'),
      ],
      '#default_value' => isset($name) ? $name : NULL,
      '#required' => TRUE,
    ],
    'location' => [
      '#type' => 'textfield',
      '#title' => t('Address'),
      '#attributes' => [
        'placeholder' => t('Enter the auditorium address'),
      ],
      '#default_value' => isset($location) ? $location : NULL,
      '#required' => TRUE,
    ],
    'status' => [
      '#type' => 'radios',
      '#title' => t('Status'),
      '#options' => [
        t('Inactive'),
        t('Active'),
      ],
      '#default_value' => isset($status) ? $status : 1,
    ],
    'number_floors' => [
      '#type' => 'select',
      '#title' => t('Number floors'),
      '#options' => [
        0 => t('Undefined'),
        1 => 1,
        2 => 2,
        3 => 3,
        4 => 4,
        5 => 5,
      ],
      '#ajax' => [
        'callback' => 'tbwa_reservations_conf',
        'wrapper' => 'content-config-floor',
      ],
      '#default_value' => isset($number_floors) ? $number_floors : 0,
    ],
  ];

  // Fields to configure chairs by floor.
  $form['config_chairs'] = [
    '#type' => 'fieldset',
    '#title' => t('Chairs config'),
    'config_floors' => [
      '#type' => 'container',
      '#tree' => TRUE,
      'content' => [
        '#prefix' => '<div id="content-config-floor">',
        '#suffix' => '</div>',
      ],
    ],
  ];

  // Submittion.
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  ];

  // Load the floor configuration.
  if ((isset($form_state['triggering_element']) && $form_state['triggering_element']['#name'] == 'number_floors') || isset($number_floors)) {
    // For edition.
    if (!isset($number_floors)) {
      $number_floors = $form_state['values']['number_floors'];
    }

    for ($i = 1; $i <= $number_floors; $i++) {
      $number_rows = $number_columns = $structure_chairs = NULL;
      if (isset($config) && isset($config[$i])) {
        $number_rows = $config[$i]['fields']['rows'];
        $number_columns = $config[$i]['fields']['columns'];
        $structure_chairs = drupal_json_encode($config[$i]['structure_chairs']);
      }

      // Add the fields to configure the floors.
      $form['config_chairs']['config_floors']['content'][$i] = [
        '#type' => 'fieldset',
        '#title' => t('Configuration to floor @floor', ['@floor' => $i]),
        'fields' => [
          '#prefix' => '<div class="container-fields">',
          '#suffix' => '</div>',
          'rows' => [
            '#type' => 'textfield',
            '#title' => t('Rows number'),
            '#attributes' => [
              'class' => [
                'is-numeric',
                'rows-floor',
              ],
              'placeholder' => t('Enter the rows number'),
            ],
            '#default_value' => isset($number_rows) ? $number_rows : 0,
          ],
          'columns' => [
            '#type' => 'textfield',
            '#title' => t('Columns number'),
            '#attributes' => [
              'class' => [
                'is-numeric',
                'columns-floor',
              ],
              'placeholder' => t('Enter the columns number'),
            ],
            '#default_value' => isset($number_columns) ? $number_columns : 0,
          ],
          'create' => [
            '#markup' => l(t('See structure'), '/', [
              'attributes' => [
                'class' => ['btn-create-structure'],
              ],
            ]),
          ],
        ],
        'structure' => [
          '#prefix' => '<div class="container-structure">',
          '#suffix' => '</div>',
        ],
        'structure_chairs' => [
          '#type' => 'hidden',
          '#attributes' => [
            'class' => ['structure-chairs'],
          ],
          '#default_value' => isset($structure_chairs) ? $structure_chairs : NULL,
        ],
      ];
    }
  }
  return $form;
}

/**
 * Implementation of hook_form_submit().
 */
function tbwa_reservations_conf_auditoriums_form_submit($form, $form_state) {
  // Información del auditorio.
  $values = $form_state['values'];

  $name = filter_xss($values['name']);
  $location = filter_xss($values['location']);
  $status = $values['status'];
  $config_floors = [];

  // Validate exist chairs configuration.
  if (isset($values['config_floors'])) {
    $config_floors = $values['config_floors']['content'];
    $number_chairs = 0;
    foreach ($config_floors as $key => &$value) {
      $number_chairs += substr_count($value['structure_chairs'], 'true');
      $value['structure_chairs'] = drupal_json_decode($value['structure_chairs']);
    }
    $config_floors['number_chairs'] = $number_chairs;
  }

  // Save the information.
  $record = [
    'name' => $name,
    'config_chairs' => drupal_json_encode($config_floors),
    'status' => $status,
    'location' => $location,
  ];

  // Update data.
  if (isset($form_state['storage']['auditorium_id'])) {
    db_update('tbwa_auditoriums')
      ->fields($record)
      ->condition('id', $form_state['storage']['auditorium_id'])
      ->execute();
    $message = t('The auditorium has been updated sucesfully');
  }
  else {
    drupal_write_record('tbwa_auditoriums', $record);
    $message = t('The auditorium has been created sucesfully');
  }

  drupal_set_message($message);
}

/**
 * Ajax callback.
 */
function tbwa_reservations_conf($form, $form_state) {

  return $form['config_chairs']['config_floors'];
}

/**
 * Ajax callback.
 *
 * Create auditorium structure with the Post variables.
 */
function tbwa_reservations_conf_auditoriums_get_structure() {
  if (isset($_POST['rows']) && isset($_POST['columns']) && $_POST['rows'] > 0  && $_POST['columns'] > 0) {
    $chairRows = $_POST['rows'] + 2;
    $chairColumns = $_POST['columns'] + 2;
    $order = NULL;
    if (isset($_POST['order']) && $_POST['order'] != '') {
      $order = drupal_json_decode($_POST['order']);
    }

    $build = [
      '#prefix' => '<div class="auditorium-structure">',
      '#suffix' => '</div>',
      // Utility information.
      'information' => [
        '#prefix' => '<div class="informstion">',
        '#suffix' => '</div>',
        '#markup' => t('Click en P para habilitar un corredor'),
      ],
      // Emergency exit.
      'emergency' => [
        '#prefix' => '<div class="emergency">',
        '#suffix' => '</div>',
        '#markup' => l(t('Set emergency exit'), '/', [
          'attributes' => [
            'class' => ['active-emergency'],
          ],
        ]),
      ],
      // Chairs.
      'content' => [
        '#prefix' => '<div class="auditorium-content">',
        '#suffix' => '</div>',
      ],
    ];

    // Each to rows.
    $header = [];
    $rows = [];

    for ($i = 1; $i <= $chairRows; $i++) {
      // Each to columns.
      for ($j = 1; $j <= $chairColumns; $j++) {
        $nameRow = $i + 94;
        $nameRow = chr($nameRow);
        $nameColumn = $j - 2;
        $nameField = "{$nameRow}-{$nameColumn}";
        // Add header.
        if ($i == 1) {
          if ($j > 2) {
            $header[] = $j - 2;
          }
          else {
            $header[] = '';
          }
        }
        elseif ($i == 2) {
          if ($j > 2) {
            $rows[$i][] = '<span class="item-row item-corredor column-' . $nameColumn . '" fieldstodisable="column-' . $nameColumn . '">P</span>';
          }
          else {
            $rows[$i][] = [''];
          }
        }
        // Add chairs.
        else {
          // Add name rows.
          if ($j == 1) {
            $rows[$i] = [
              $nameRow,
            ];
          }
          elseif ($j == 2) {
            $rows[$i][] = '<span class="item-row item-corredor row-' . $nameRow . '" fieldstodisable="row-' . $nameRow . '">P</span>';
          }
          else {
            $classField = "row-{$nameRow} column-{$nameColumn} field-{$nameField}";
            $valueChair = isset($order[$i - 1][$j - 3]) ? $order[$i - 1][$j - 3] : NULL;

            if ($valueChair === 'e') {
              $classField .= ' emergency-exit';
            }
            elseif (!$valueChair) {
              $classField .= ' chair-disabled';
            }
            $rows[$i][] = [
              'data' => '<span class="item-chair ' . $classField . '"></span>',
              'class' => ['is-chair'],
            ];
          }
        }
      }
    }

    // Return the table structure.
    $build['content']['table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];

    drupal_json_output(render($build));
  }
}
