<?php

/**
 * @file
 * Auditorium list page.
 */

/**
 * Function tbwa_reservations_list_auditoriums_page().
 *
 * Show the list autitoriums.
 */
function tbwa_reservations_list_auditoriums_page() {
  // Get the auditoriums.
  $querys = tbwa_reservations_load_querys();
  $auditoriums = $querys->getAuditoriums();

  if (!empty($auditoriums)) {
    $header = [
      t('Auditorium name'),
      t('Location'),
      t('State'),
      t('Actions'),
    ];
    $rows = [];
    foreach ($auditoriums as $key => $value) {
      $status = t('Active');
      if (!$value->status) {
        $status = t('Inactive');
      }
      $rows[] = [
        $value->name,
        $value->location,
        $status,
        l(t('Edit'), 'admin/config/system/auditoriums/config/' . $value->id),
      ];
    }
    $build = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $rows,
    ];
  }
  else {
    $build = [
      '#prefix' => '<div class="no-results">',
      '#suffix' => '</div>',
      '#markup' => t('No found auditoriums') . ' ' . l(t('Add auditorium'), 'admin/config/system/auditoriums/config'),
    ];
  }

  return $build;
}
