<?php

/**
 * @file
 * Settings of module.
 */

/**
 * Implements hook_form().
 */
function tbwa_reservations_settings_form($form, &$form_state) {
  // Get the content types.
  $content_types = node_type_get_types();
  $options = [];
  foreach ($content_types as $machine_name => $value) {
    $options[$machine_name] = $value->name;
  }

  // Tipo de contenido que activa la reserva de auditorios.
  $form['tbwa_reservations_settings_content_types'] = [
    '#type' => 'checkboxes',
    '#title' => t('Content types'),
    '#description' => t("Select the content type to active auditorium's reservations"),
    '#options' => $options,
    '#default_value' => variable_get('tbwa_reservations_settings_content_types', NULL),
  ];

  // Limite de reservas por usuarios.
  $form['tbwa_reservations_settings_limit_user'] = [
    '#type' => 'textfield',
    '#title' => t('Reserves limit for users'),
    '#description' => t('textfield'),
    '#attributes' => [
      'placeholder' => t('Enter limit'),
    ],
    '#default_value' => variable_get('tbwa_reservations_settings_limit_user', NULL),
  ];

  return system_settings_form($form);
}
