<?php

/**
 * @file
 * Administration events page.
 */

/**
 * Function tbwa_reservations_admin_events().
 */
function tbwa_reservations_admin_events() {
  $filterForm = drupal_get_form('tbwa_reservations_admin_events_form');
  $build = [
    '#prefix' => '<div class="container-page page-list-events">',
    '#suffix' => '</div>',
    'filters' => [
      '#prefix' => '<div class="form-filters">',
      '#suffix' => '</div>',
      '#markup' => render($filterForm),
    ],
  ];

  if (isset($_GET['in'])) {
    $filters = [
      'date' => [
        'value' => [
          'in' => $_GET['in'],
          'fn' => $_GET['in'] + 86400,
        ],
        'field' => 'start_date',
        'oper' => 'BETWEEN',
      ],
    ];

    $querys = tbwa_reservations_load_querys();
    $result = $querys->getEventsFilter($filters);

    if (!empty($result)) {
      $header = [
        t('Name'),
        t('Start date'),
        t('Finish date'),
        t('type'),
        t('Auditorium'),
      ];
      $rows = [];
      foreach ($result as $key => $event) {
        // Event information.
        $node = node_load($event->node_id);
        $initialDate = date('Y, M j h:i a', $event->start_date);
        $finishDate = date('Y, M j h:i a', $event->finish_date);
        $eventType = $event->type;
        // Auditorium Information.
        $auditorium = $querys->getAuditoriumsInfo($event->auditorium_id);

        $rows[] = [
          $node->title,
          $initialDate,
          $finishDate,
          $eventType,
          $auditorium->name,
        ];
      }

      $build['table'] = [
        '#theme' => 'table',
        '#header' => $header,
        '#rows' => $rows,
      ];
    }
    else {
      $build['message'] = [
        '#prefix' => '<div class="events-empty">',
        '#suffix' => '</div>',
        '#markup' => t('Not found results'),
      ];
    }
  }

  return $build;
}

/**
 * Implements hook_form().
 */
function tbwa_reservations_admin_events_form($form, &$form_state) {
  drupal_add_js('https://code.jquery.com/ui/1.10.1/jquery-ui.js', 'external');
  drupal_add_js(drupal_get_path('module', 'tbwa_reservations') . '/libraries/tbwa_reservations_config.js');

  $form['initial'] = array(
    '#type' => 'textfield',
    '#title' => t('Date'),
    '#attributes' => [
      'class' => ['filter-date'],
      'placeholder' => t('Initial date'),
    ],
    '#required' => TRUE,
  );
  $form['submit'] = [
    '#type' => 'submit',
    '#value' => t('Filter'),
  ];

  return $form;
}

/**
 * Implementation of hook_form_submit().
 */
function tbwa_reservations_admin_events_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $initial = strtotime($values['initial'] . ' 00:00:00');

  drupal_goto('admin/config/events', [
    'query' => [
      'in' => $initial,
    ],
  ]);
}
