<?php

/**
 * @file
 * Reservations page.
 */

 /**
  * Function tbwa_reservations_client_page().
  *
  * @param object $node
  *   The node.
  */
function tbwa_reservations_client_page($node) {
  $event = tbwa_reservations_get_by_node($node->nid);
  // Event information.
  $event_id = $event->id;
  $startDate = date('F j, Y, g:i a', $event->start_date);
  $duration = ($event->finish_date - $event->start_date) / 3600;
  $duration .= ' ' . t('hours');

  // Verify if uid exist.
  $uid = tbwa_reservations_create_session();

  // Validate accept more reserves.
  $limitReserves = variable_get('tbwa_reservations_settings_limit_user', 3);
  $currentReservations = tbwa_reservations_get_by_uid($uid, $event_id);
  $different = $limitReserves - count($currentReservations);

  $classReservations = 'reservation';
  $reservations = tbwa_reservations_get_empty_by_uid($uid, $event_id);

  if (!empty($reservations)) {
    // Class to show modal form.
    $classReservations = 'have-reservation';
  }

  $form = drupal_get_form('tbwa_reservations_client_form', $event, $different);
  $build = [
    '#prefix' => '<div class="container-page-reservations ' . $classReservations . '">',
    '#suffix' => '</div>',
    // Node body.
    'body' => [
      '#prefix' => '<div class="node-body">',
      '#suffix' => '</div>',
      '#markup' => substr($node->body[LANGUAGE_NONE][0]['value'], 0, 250) . '...',
    ],
    // Resumen del evento.
    'summary' => [
      '#prefix' => '<div class="summary">',
      '#suffix' => '</div>',
      'event_name' => [
        '#prefix' => '<h4 class="event-name">',
        '#suffix' => '</h4>',
        '#markup' => '<b>' . t('Event name') . '</b> ' . $node->title,
      ],
      'date' => [
        '#prefix' => '<div class="event-date">',
        '#suffix' => '</div>',
        '#markup' => '<b>' . t('Date') . '</b> ' . $startDate,
      ],
      'duration' => [
        '#prefix' => '<div class="event-duration">',
        '#suffix' => '</div>',
        '#markup' => '<b>' . t('Duration') . '</b> ' . $duration,
      ],
      'event_type' => [
        '#prefix' => '<small class="event-type">',
        '#suffix' => '</small>',
        '#markup' => t('@type', ['@type' => $event->type]),
      ],
    ],
    // Chairs descriptions.
    'header' => [
      '#prefix' => '<div class="header-auditorium">',
      '#suffix' => '</div>',
      'title' => [
        '#prefix' => '<h4>',
        '#suffix' => '</h4>',
        '#markup' => t('Select the chairs'),
      ],
      'description' => [
        '#prefix' => '<div class="chairs-description">',
        '#suffix' => '</div>',
        'available' => [
          '#prefix' => '<span class="chair-available">',
          '#suffix' => '</span>',
          '#markup' => t('Chair available'),
        ],
        'selected' => [
          '#prefix' => '<span class="chair-selected">',
          '#suffix' => '</span>',
          '#markup' => t('Chair selected'),
        ],
        'busy' => [
          '#prefix' => '<span class="chair-busy">',
          '#suffix' => '</span>',
          '#markup' => t('Chair busy'),
        ],
      ],
    ],
    // Auditoriums structure.
    'form' => [
      '#prefix' => '<div class="wrapper-auditorium">',
      '#suffix' => '</div>',
      '#markup' => render($form),
    ],
  ];

  return $build;
}

/**
 * Implements hook_form().
 */
function tbwa_reservations_client_form($form, &$form_state, $event, $different) {
  // Get limit reserves.
  $limitReserves = variable_get('tbwa_reservations_settings_limit_user', 3);

  $optionsLimit = [];
  for ($i = 1; $i <= $limitReserves; $i++) {
    $defaultLimit = $i;
    $optionsLimit[$i] = $i;
  }

  // Get chairs structure config.
  $querys = tbwa_reservations_load_querys();
  $auditorium = $querys->getAuditoriumsInfo($event->auditorium_id);
  $config_fields = $auditorium->config_chairs;
  $config = drupal_json_decode($config_fields);

  $options = [];
  $defaultOption = 1;
  foreach ($config as $key => $value) {
    if (is_array($value)) {
      if (empty($options)) {
        $defaultOption = $key;
      }
      $options[$key] = t('Floor') . " {$key}";
    }
  }

  $data_event['nid'] = $event->node_id;
  $auditorium = tbwa_reservations_auditorium_structure($event->auditorium_id, $data_event);

  $form['places'] = [
    '#type' => 'select',
    '#title' => t('Select quantity places'),
    '#options' => $optionsLimit,
    '#default_value' => $defaultLimit,
  ];
  $form['floors'] = [
    '#type' => 'radios',
    '#options' => $options,
    '#default_value' => $defaultOption,
  ];
  $form['table'] = [
    '#markup' => render($auditorium),
  ];
  $form['path_reserve'] = [
    '#prefix' => '<div class="container-path">',
    '#suffix' => '</div>',
    '#markup' => l(t('Reserve'), 'modal-reserve-auditorium/finish/' . $event->id, [
      'attributes' => [
        'class' => ['btn', 'btn-success', 'ctools-use-modal'],
      ],
    ]),
  ];

  return $form;
}

/**
 * Function tbwa_reservations_client_modal_reserve().
 *
 * @param int $event_id
 *   Event identificator.
 */
function tbwa_reservations_client_modal_reserve($event_id) {
  ctools_include('modal');
  ctools_include('ajax');

  // Como mostrar un formulario en un popup.
  $form_state = [
    'ajax' => TRUE,
    'title' => t('Reserve chair'),
    'event_id' => $event_id,
  ];
  $output = ctools_modal_form_wrapper('tbwa_reservations_client_modal_reserve_form', $form_state);
  if (!empty($form_state['executed'])) {
    $commands[] = ctools_ajax_command_reload();
    print ajax_render($commands);
    exit;
  }
  print ajax_render($output);
  exit;
}

/**
 * Implements hook_form().
 */
function tbwa_reservations_client_modal_reserve_form($form, $form_state) {
  $uid = $_SESSION['uid'];
  $event_id = $form_state['event_id'];
  $reservations = tbwa_reservations_get_empty_by_uid($uid, $event_id);
  if (!empty($reservations)) {
    $reservation = reset($reservations);

    $form['header'] = [
      '#prefix' => '<div class="header-form">',
      '#suffix' => '</div>',
      '#markup' => t('Reservation data for the !chairName chair', [
        '!chairName' => '<span class="chair-name">' . $reservation->chair . '</span>',
      ]),
    ];
    $form['fields'] = [
      '#tree' => TRUE,
      'name' => [
        '#type' => 'textfield',
        '#title' => t('Name'),
        '#title_display' => 'invisible',
        '#attributes' => ['placeholder' => t('Name')],
      ],
      'lastname' => [
        '#type' => 'textfield',
        '#title' => t('Lastname'),
        '#title_display' => 'invisible',
        '#attributes' => ['placeholder' => t('Lastname')],
      ],
      'identification' => [
        '#type' => 'textfield',
        '#title' => t('Identification'),
        '#title_display' => 'invisible',
        '#attributes' => ['placeholder' => t('Identification')],
      ],
      'email' => [
        '#type' => 'textfield',
        '#title' => t('Email'),
        '#title_display' => 'invisible',
        '#attributes' => ['placeholder' => t('Email')],
      ],
      'id' => [
        '#type' => 'hidden',
        '#value' => $reservation->id,
      ],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => t('Save'),
    ];

    return $form;
  }
}

/**
 * Implements hook_form_submit().
 */
function tbwa_reservations_client_modal_reserve_form_submit($form, $form_state) {
  $fields = $form_state['values']['fields'];
  $fields['status'] = TBWA_RESERVATIONS_RESERVE_BY_CONFIRM;

  db_update('tbwa_reservations')
    ->fields($fields)
    ->condition('id', $fields['id'])
    ->execute();

  // Successfully message.
  drupal_set_message(t('The reservations has been finish successfully'));
}
